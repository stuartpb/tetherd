# tetherd

A tiny daemon to toggle radios off in the presence of a tethered connection (ie. disable Wi-Fi if an Ethernet cable is inserted).